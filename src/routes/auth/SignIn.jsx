import React from 'react';
import { signInWithGooglePopup } from "../../utils/firebase";

const SignIn = () => {
  const logGoogleUser = async () => {
    const response = await signInWithGooglePopup()
    console.log(response)
  }
  return (
    <div>
      <button onClick={logGoogleUser}>Sign with google</button>
      <h1>Sign In</h1>
    </div>
  );
};

export default SignIn;