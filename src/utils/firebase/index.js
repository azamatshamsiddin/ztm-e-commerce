import { initializeApp } from "firebase/app"
import { getAuth, GoogleAuthProvider, signInWithPopup } from "firebase/auth"

import {
  getDoc,
  setDoc,
  doc,
  getFirestore
} from 'firebase/firestore'

const firebaseConfig = {
  apiKey: "AIzaSyCbd1L4jY5kb5COE6dX5zJ26_cIWMXD-7w",
  authDomain: "crwn-clothes-demo.firebaseapp.com",
  projectId: "crwn-clothes-demo",
  storageBucket: "crwn-clothes-demo.appspot.com",
  messagingSenderId: "259785944779",
  appId: "1:259785944779:web:d0c4d0abfa47b2ff7cf54e"
};



// Initialize Firebase
const firebaseApp = initializeApp(firebaseConfig);

const provider = new GoogleAuthProvider();

provider.setCustomParameters({
  prompt: "select_account"
})

export const auth = getAuth()
export const signInWithGooglePopup = () => signInWithPopup(auth, provider)

export const db = getFirestore()